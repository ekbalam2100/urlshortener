This service generates short-url-names from regular-url-names. 
It is written in Julia language: https://julialang.org/

In order to run it, it needs Julia and SQLite installed
Julia reference: https://docs.julialang.org/en/v1/
SQLite reference: https://www.sqlite.org/index.html

The service needs these Julia packages: DataFrames, SQLite, Base64, HTTP, Sockets, SHA, JSON2
To add apackage in Julia, run Julia, and then press ']' and 'add name_package' and press ENTER

To start the service, run start_service.sh script

To test the service, from another terminal run testing.sh script
Testing.sh does several processes
1) clean the database
2) run 2 POST for long to short conversion
3) shows the database content
4) run GET to retrive a long name
5) run GET for a non valid short name


Alternativate can be tested running these command from the linux terminal:

1) convert a long name in a short name:
curl --header "Content-Type: application/json"   --request POST --data '{"long": "https://wiki.alpinelinux.org/wiki/K8s"}'  http://localhost:5000/

2) get the long name from a short name
curl -L http://localhost:5000/U7JRWXMn




