using DataFrames, SQLite, Base64, HTTP, Sockets, SHA, JSON2

const dburl = SQLite.DB("/home/rafael/url/urlShortener.db")

function getLong(shortname)
  # sql command
  sqlstm = "SELECT * FROM longShort WHERE shortName like '"* shortname *"';"
  # execute sql over database 'dburl' and load result in a dataframe 'res'
  res = DBInterface.execute(dburl, sqlstm)
  return DataFrame(res)
end

function insertShortLong(shortname, longname)
  sqlstm="INSERT INTO longShort VALUES ('"*shortname*"','"*longname*"')"
  DBInterface.execute(dburl,sqlstm)
end

function processPOST(req::HTTP.Request, urilen=8)
  # read (expected) json format
  json_string = JSON2.read(String(String(HTTP.payload(req))))

  # check expected format {long: long-name}
  if haskey(json_string, :long)
    longname = json_string.long
    # encode full string, initilize shortname var
    encoded, shortname = [UInt8(c) for c in base64encode(sha256(longname))], ""
    # circulate encoded to reduce unitl 'urilen'
    for i in 0:length(encoded)-1
      shortname = String(circshift(encoded, i)[1:urilen])
      # circshift(A, shifts)
      # Circularly shift, i.e. rotate, the data in an array. The second argument is a tuple or vector giving the amount to shift in each dimension, or an integer to shift only in the first dimension
    end
    # query database for 'shortname' value
    result = getLong(shortname)
    if isempty(result)
      # if does not exist in database, add it, and return results
      insertShortLong(shortname, longname)
      return HTTP.Response(200, JSON2.write("Short name: $shortname, Long name: $longname"))
    else
      # if shortname is in database, return result
      lname = result[!,:2][1] #select string from DataFrame
      return HTTP.Response(200, JSON2.write("Short name: $shortname, Long name: $lname"))
    end
  end
  HTTP.Response(400, JSON2.write("Bad request. Please POST JSON --request POST --data \'{\"long\": \"longname\"}\'"))
end

function processGET(req::HTTP.Request)
  # remove url part, keep shortname
  shortname = split(req.target, r"[^\w\d\+\\]+")[end]
  # lookup at DB
  result = getLong(shortname)
  if isempty(result) 
    return HTTP.Response(200, JSON2.write( "Short name $shortname NOT FOUND."))
  else
    lname = result[!,:2][1]
    return HTTP.Response(200, JSON2.write( "Long name: $lname"))
  end
end

function runWebServer(server, portnum) 
  # basic serverprocessing POST/GET
  router = HTTP.Router()
  HTTP.@register(router, "POST", "", processPOST)
  HTTP.@register(router, "GET", "/*", processGET)
  HTTP.serve(router, server, portnum)
end

function start()
  # initilization
  serveraddress = Sockets.localhost
  localport = 5000
  runWebServer(serveraddress, localport)
end

start()

############## extras ################

# • Tailor the service to protect privacy, since some users require anonymity

# • Provide an IDE integration, chat bot, and/or fancier web app enabling richer access to the service

# • Create a facility for backing up, mirroring, and/or archiving the URLs

# • Package the service as a deb, container, gem, or some other format for easy deployment

# • Include preventative measures against use of the service for nefarious purposes such as fraud, distribution of malware, or coordination of a bot net

# • Document the service such that a junior developer or an advanced student with little knowledge of the language you use or of web services can understand its design and become better educated




# add token api authentication

