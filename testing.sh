#!/bin/bash
# definitions
workfolder="/home/rafael/url"
database=$workfolder"/urlShortener.db"
input=$workfolder"/long-url.txt"
output=$workfolder"/short-url.txt"

printf "\n\n-------- START testing -------------\n"

# clean database
echo "Cleaning database..."
sql_res="DELETE FROM longShort"
echo "$sql_res"  | sqlite3 $database 

# show database information
echo "Database information..."
echo -e .tables | sqlite3 $database
echo -e .schema longShort | sqlite3 $database

# making some POST calls
# TODO: make a loop, issue with curl command 
printf "\n\n-------- Testing POST -------------\n"
curl --header "Content-Type: application/json"   --request POST --data '{"long": "https://wiki.alpinelinux.org/wiki/K8s"}'  http://localhost:5000/

printf "\n\n-------- Testing POST -------------\n"
curl --header "Content-Type: application/json" --request POST --data '{"long": "https://dev.to/xphoniex/how-to-create-a-kubernetes-cluster-on-alpine-linux-kcg"}'  http://localhost:5000/

# show database records
printf "\n\n-------- Database content -------------\n"
sql_res="SELECT * FROM longShort" 
echo "$sql_res"  | sqlite3 $database 

# make GET call retriving long-names
printf "\n\n-------- Testing GET -------------\n"
short="U7JRWXMn"
echo "Short name: "$short
curl -L http://localhost:5000/$short

# check non exisiting short-name
printf "\n\n-------- Testing non-exist short -------------\n"
short="U7NOTMn"
echo "Short name: "$short
curl -L http://localhost:5000/$short


printf "\n\n-------- END testing -------------\n"


